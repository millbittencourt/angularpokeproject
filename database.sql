    --------------------
    --    drop all    --
    --------------------

    drop table if exists pokemon cascade;
    drop sequence if exists pokemon_id_seq;

    drop table if exists treinador cascade;
    drop sequence if exists treinador_id_seq;

    --------------------
    --    treinador    --
    --------------------

    create sequence treinador_id_seq increment 1 minvalue 1 maxvalue 9223372036854775807 start 1 cache 1;

    create table treinador (
        id bigint default nextval('treinador_id_seq') not null,
        --id_pokemon bigint,
        email character varying(1000) not null,
        name character varying(1000) not null,
        constraint treinador_pkey primary key (id)
        --constraint id_pokemon_fk foreign key (id_pokemon) references pokemon (id) match simple
    );

    insert into treinador (name, email) values ('Jamille', 'mille@mille');

    -----------------
    --    pokemon    --
    -----------------

    create sequence pokemon_id_seq increment 1 minvalue 1 maxvalue 9223372036854775807 start 1 cache 1;

    create table pokemon (
        id bigint default nextval('pokemon_id_seq') not null,
      --  pokeId bigint,
        name character varying(500) not null,
        height int not null,
        speciesName character varying(500) not null,
        TreinadorId bigint,
        constraint pokemon_pkey primary key (id),
        constraint id_treinador_fk foreign key (treinadorId) references treinador (id) match simple
    );

    insert into pokemon (name, height, speciesname, Treinadorid) values ('Pokemon', 1, 'Pokemon', null);

select * from treinador