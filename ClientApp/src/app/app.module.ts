import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination'; 

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { ListarPokemonComponent } from './listar-pokemon/listar-pokemon.component';
import { AddPokemonComponent } from './add-pokemon/add-pokemon.component';
import { AddTreinadorComponent } from './add-treinador/add-treinador.component';
import { ListarTreinadorComponent } from './listar-treinador/listar-treinador.component';
import { AddPokemonTreinadorComponent } from './add-pokemon-treinador/add-pokemon-treinador.component';

  
@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    ListarPokemonComponent,
    AddPokemonComponent,
    AddTreinadorComponent,
    ListarTreinadorComponent,
    AddPokemonTreinadorComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    HttpModule,
    FormsModule,
    NgxPaginationModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'listar-pokemon', component: ListarPokemonComponent },
      { path: 'add-pokemon', component: AddPokemonComponent },
      { path: 'add-treinador', component: AddTreinadorComponent },
      { path: 'listar-treinador', component: ListarTreinadorComponent },
      { path: 'add-pokemon-treinador', component: AddPokemonTreinadorComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
