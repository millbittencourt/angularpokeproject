import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { Pokemon } from '../models/pokemon';

@Component({
  selector: 'app-listar-pokemon',
  templateUrl: './listar-pokemon.component.html'
})

export class ListarPokemonComponent {
  public pokemons: ListaPokemons[];
  public pokemon: Pokemon[];
  p: number = 1;
  
  pesquisar(nome: string){
    this.pokemons = null;
    this.http.get<Pokemon[]>(this.baseUrl + 'api/Pokemon/ListarPokemon/' + nome)
    .subscribe(result => { this.pokemon = result; },
      () => alert('Pokemon não encontrado'));
  }

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    http.get<ListaPokemons[]>(baseUrl + 'api/Pokemon/ListarPokemon')
      .subscribe(result => { this.pokemons = result; },
        error => console.error(error));
  }
}

interface ListaPokemons {
  name: string;
  url: string;
}

interface Pokemon {
      id: number;
      name: string;
      height: number;
      speciesName: string;
  }  
  