import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Pokemon, Treinador } from '../models/pokemon';

@Component({
  selector: 'app-add-pokemon-treinador',
  templateUrl: './add-pokemon-treinador.component.html'
})
export class AddPokemonTreinadorComponent {
  public params: Parametros;
  public pokemons: Pokemon;
  public treinadores: Treinador;

  onSubmit(form: NgForm) {
    console.log("To no submit");
    console.log(form.value.nomeTreinador);
    this.putPokemon(form.value)
    .subscribe(res => {
      alert(form.value.nomeTreinador + " agora tem " + form.value.nomePokemon);
      this.resetForm(form);
    });
  }

  putPokemon(formValue) {
    return this.http.put<Parametros>(this.baseUrl + 'api/Treinador/AdicionarPokemon/', formValue);
  }

  resetForm(form?: NgForm) {
    form.resetForm(); 
  }

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { 
    http.get<Pokemon>(baseUrl + 'api/Pokemon/ListarPokemonBanco')
    .subscribe(result => { this.pokemons = result;});

    http.get<Treinador>(baseUrl + 'api/Treinador/ListarTreinador')
    .subscribe(result => { this.treinadores = result; });

  }
}

interface Parametros {
  nomeTreinador: string,
  nomePokemon: string;
}


