import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-pokemon',
  templateUrl: './add-pokemon.component.html'
})
export class AddPokemonComponent {
  //public poke: Pokemon;

  onSubmit(form: NgForm) {
   // alert(form.value.nome);
    this.pesquisa(form.value.nome, form);
  }

  pesquisa(param, form) {
    this.http.get(this.baseUrl + 'api/Pokemon/BuscarPokemon/' + param)
    .subscribe(dados => {this.preencheForm(dados, form)},
     () => alert("Pokemon não encontrado. =("));
  }
  
  preencheForm(dados, form) {
    console.log("Preencher formulario do " + dados.name);
    form.setValue({
      nome: form.value.nome,
      id: dados.id,
      name: dados.name,
      height: dados.height,
      speciesName: dados.speciesName
    });
  }
  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }
}

//interface Pokemon {
//  id: number;
//  name: string;
//  height: number;
//  speciesName: string;
//}

