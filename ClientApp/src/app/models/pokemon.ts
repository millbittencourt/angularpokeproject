
export interface Pokemon {
    id: number;
    name: string;
}  

export interface Treinador {
    id: number;
    name: string;
}  

