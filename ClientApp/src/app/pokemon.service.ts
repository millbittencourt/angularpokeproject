import { Injectable } from '@angular/core';
import { Http } from '@angular/http'

@Injectable()
export class PokemonService {

  constructor(private http: Http) { }

  getPokemonData(nome) {
    var poke = this.http.get("https://pokeapi.co/api/v2/pokemon/" + nome);
    return poke;
  }
}
