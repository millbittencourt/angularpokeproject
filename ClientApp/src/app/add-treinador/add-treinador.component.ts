import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-treinador',
  templateUrl: './add-treinador.component.html'
})
export class AddTreinadorComponent {
  public treinador: Treinador;

  onSubmit(form: NgForm) {
    this.postTreinador(form.value)
      .subscribe(res => {
        console.log('treinaador regsitred');
        this.resetForm(form);
      });
  }
  resetForm(form?: NgForm) {
    form.resetForm(); 
  }

  postTreinador(formValue: Treinador) {
    return this.http.post<Treinador>(this.baseUrl + 'api/Treinador/AdicionarTreinador/', formValue);
  }

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }
}

interface Treinador {
  name: string,
  email: string
}

