import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-listar-treinador',
  templateUrl: './listar-treinador.component.html'
})
export class ListarTreinadorComponent {
  public treinadores: ListaTreinadores[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<ListaTreinadores[]>(baseUrl + 'api/Treinador/ListarTreinador')
      .subscribe(result => { this.treinadores = result; },
        error => console.error(error));
  }
}

interface ListaTreinadores {
  id: number;
  name: string;
  height: number;
  speciesName: string;
}
