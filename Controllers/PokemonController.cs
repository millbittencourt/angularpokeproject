﻿using Microsoft.AspNetCore.Mvc;
using PokeProject.Data;
using PokeProject.Models;
using PokeProject.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PokeProject.Controllers
{
    [Route("api/[controller]")]
    public class PokemonController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public async Task<NamedApiResourceList<Pokemon>> BuscarPokemons()
        {//964 me
            Console.WriteLine("BuscarPokemonsSemParam");

            // List<Pokemon> pokemons = new List<Pokemon>();
            NamedApiResourceList<Pokemon> pokemonsLista = null;
            List<string> pokemons = new List<string>();

            PokeApiClient client = new PokeApiClient();

            pokemonsLista = await client.GetNamedResourcePageAsync<Pokemon>(964, 0);
            /* foreach (var pokemon in pokemonsLista.Results)
             {
                // var poke = await client.GetResourceAsync<Pokemon>(pokemon.Name);
                 pokemons.Add(pokemon.Name);
             }*/

            return pokemonsLista;
        }

        [HttpGet("[action]/{param}")]
        public async Task<Pokemon> BuscarPokemon(string param)
        {
            Pokemon pokemon = null;
            if (param != null)
            {
                PokeApiClient client = new PokeApiClient();
                pokemon = await client.GetResourceAsync<Pokemon>(param);
            }

              if (pokemon != null)
              {
                  if (!verificaSeExiste(pokemon, param))
                  {
                      using (var db = new PokeContext())
                      {
                          Pokemon poke = new Pokemon();
                          poke.Id = pokemon.Id;
                          poke.Name = pokemon.Name;
                          poke.Height = pokemon.Height;
                          poke.Species = pokemon.Species;
                          poke.SpeciesName = pokemon.SpeciesName;

                          var teste = db.Pokemons.Add(poke);
                          var num = db.SaveChanges();
                      }
                  }
              } 
            var p = new Pokemon
            {
                Id = pokemon.Id,
                Name = pokemon.Name,
                Height = pokemon.Height,
                Species = pokemon.Species,
                SpeciesName = pokemon.SpeciesName
            };
            return p;
        }

        public bool verificaSeExiste(Pokemon pokemon, string param)
        {
            var db = new PokeContext();

            var qtd = db.Pokemons.Where(x => x.Name == param || x.Id.ToString() == param).Count();
            if (qtd > 0)
                return true;
            return false;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<Object>> ListarPokemon()
        {
            NamedApiResourceList<Pokemon> pokemons = await BuscarPokemons();

            var lista = Enumerable.Range(0, pokemons.Results.Count).Select(index => new
            {
                Name = pokemons.Results[index].Name,
                Url = pokemons.Results[index].Url
            });
            return lista;
        }

        [HttpGet("[action]/{param}")]
        public async Task<IEnumerable<Pokemon>> ListarPokemon(string param)
        {
            Pokemon pokemon = await BuscarPokemon(param);
            List<Pokemon> pokemons = new List<Pokemon>();
            pokemons.Add(pokemon);

            var lista = Enumerable.Range(0, pokemons.Count).Select(index => new Pokemon
            {
                Id = pokemons[index].Id,
                Name = pokemons[index].Name,
                Height = pokemons[index].Height,
                Species = pokemons[index].Species,
                SpeciesName = pokemons[index].SpeciesName
            });
            return lista;
        }

        [HttpGet("[action]")]
        public IEnumerable<Pokemon> ListarPokemonBanco()
        {
            List<Pokemon> pokemons = new List<Pokemon>();

            using (var db = new PokeContext())
            {
                foreach (var pokemon in db.Pokemons)
                {
                    pokemons.Add(pokemon);
                }
            }

            var lista = Enumerable.Range(0, pokemons.Count).Select(index => new Pokemon
            {
                Id = pokemons[index].Id,
                Name = pokemons[index].Name,
            });
            return lista;
        }
    }
}