using Microsoft.AspNetCore.Mvc;
using PokeProject.Context;
using PokeProject.Models;
using PokeProject.Controllers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace PokeProject.Controllers
{
    [Route("api/[controller]")]
    public class TreinadorController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("[action]")]
        public void AdicionarTreinador([FromBody] Treinador treinador)
        {
            Console.WriteLine("AdicionarTreinador");
            if (!verificaSeExiste(treinador))
            {
                using (var db = new PokeContext())
                {
                    Treinador t = new Treinador();
                    t.Name = treinador.Name;
                    t.Email = treinador.Email;

                    db.Treinadores.Add(t);
                    db.SaveChanges();
                }
            }
        }

        public bool verificaSeExiste(Treinador treinador)
        {
            var db = new PokeContext();
            var count = db.Treinadores.Where(x => x.Name == treinador.Name && x.Email == treinador.Email).Count();

            if (count > 0)
                return true;
            return false;
        }

        [HttpGet("[action]")]
        public IEnumerable<Pokemon> ListarPokemons(Treinador treinador)
        {
            List<Pokemon> pokemons = new List<Pokemon>();
            //buscar os pokemons do treinador x
            foreach (var pokemon in treinador.Pokemons)
            {
                pokemons.Add(pokemon);
            }

            var lista = Enumerable.Range(0, pokemons.Count).Select(index => new Pokemon
            {
                Id = pokemons[index].Id,
                Name = pokemons[index].Name,
                Height = pokemons[index].Height,
                Species = pokemons[index].Species,
                SpeciesName = pokemons[index].SpeciesName
            });
            return lista;
        }

        [HttpPut("[action]")]
        public void AdicionarPokemon([FromBody] NovoPokeTreinaReq parames)
        { //salvar tudo toLower();
            using (var db = new PokeContext())
            {
                var treinador = db.Treinadores.FirstOrDefault(x => x.Name == parames.nomeTreinador);
                var pokemon = db.Pokemons.FirstOrDefault(x => x.Name == parames.nomePokemon);

                if (pokemon == null || treinador == null)
                {
                    throw new ArgumentNullException("Não existe esse treinador ou pokemon no banco. Adicione primeiro, e depois volte aqui =D");
                }

                // if(pokemon.TreinadorId != null){
                //     Pokemon p = new Pokemon(pokemon.Id, pokemon.Name, pokemon.Height, pokemon.SpeciesName, treinador.Id);
                //     db.Pokemons.Add(p);
                // } else {
                    pokemon.TreinadorId = treinador.Id;
                    db.Pokemons.Update(pokemon);

                // }


                db.SaveChanges();
            }
        }
        
        [HttpGet("[action]")]
        public IEnumerable<Treinador> ListarTreinador()
        {
            List<Treinador> treinadores = new List<Treinador>();

            using (var db = new PokeContext())
            {
                foreach (var treinador in db.Treinadores)
                {
                    treinadores.Add(treinador);
                }
            }

            var lista = Enumerable.Range(0, treinadores.Count).Select(index => new Treinador
            {
                Id = treinadores[index].Id,
                Name = treinadores[index].Name,
                Email = treinadores[index].Email
            });
            return lista;
        }


    }
}