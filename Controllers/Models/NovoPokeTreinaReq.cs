using System.Runtime.Serialization;

namespace PokeProject.Controllers.Models
{

 [DataContract]
    public class NovoPokeTreinaReq
    {
        [DataMember(Name = "nomePokemon", IsRequired = true)]
        public string nomePokemon { get; set; }

        [DataMember(Name = "nomeTreinador", IsRequired = true)]
        public string nomeTreinador { get; set; }
    }

}