﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using PokeProject.Models;


namespace PokeProject.Data
{
    public class PokeApiClient
    {

        private readonly HttpClient _client;
        private readonly Uri _baseUri = new Uri("https://pokeapi.co/api/v2/");
        private readonly CacheManager _cacheManager = new CacheManager();

        public PokeApiClient()
        {
            _client = new HttpClient() { BaseAddress = _baseUri };
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        private async Task<T> GetResourcesWithParamsAsync<T>(string apiParam) where T : ResourceBase
        {
            // lowercase the resource name as the API doesn't recognize upper case and lower case as the same
            string sanitizedApiParam = apiParam.ToLowerInvariant();

            string apiEndpoint = GetApiEndpointString<T>();
            HttpResponseMessage response = await _client.GetAsync($"{apiEndpoint}/{sanitizedApiParam}");
            response.EnsureSuccessStatusCode();

            string resp = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(resp);
        }

        //returns the link of the api
        private string GetApiEndpointString<T>()
        {
            PropertyInfo propertyInfo = typeof(T).GetProperty("ApiEndpoint", BindingFlags.Static | BindingFlags.NonPublic);
            return propertyInfo.GetValue(null).ToString();
        }

        public async Task<T> GetResourceAsync<T>(int id) where T : ResourceBase
        {
            T resource = _cacheManager.Get<T>(id);
            if (resource == null)
            {
                resource = await GetResourcesWithParamsAsync<T>(id.ToString());
                _cacheManager.Store(resource);
            }

            return resource;
        }

        /// <summary>
        /// Gets a resource by name; resource is retrieved from cache if possible. This lookup
        /// is case insensitive.
        /// </summary>
        /// <typeparam name="T">The type of resource</typeparam>
        /// <param name="name">Name of resource</param>
        /// <returns>The object of the resource</returns>
        public async Task<T> GetResourceAsync<T>(string name) where T : ResourceBase
        {
            string sanitizedName = name
                .Replace(" ", "-")
                .Replace("'", "")   
                .Replace(".", "");     

            T resource = _cacheManager.Get<T>(sanitizedName);
            if (resource == null)
            {
                resource = await GetResourcesWithParamsAsync<T>(sanitizedName);
                _cacheManager.Store(resource);
            }

            return resource;
        }

        public async Task<NamedApiResourceList<T>> GetNamedResourcePageAsync<T>() where T : NamedApiResource
        {
            string resp = await GetPageAsync<T>();
            return JsonConvert.DeserializeObject<NamedApiResourceList<T>>(resp);
        }

        public async Task<NamedApiResourceList<T>> GetNamedResourcePageAsync<T>(int limit, int offset) where T : NamedApiResource
        {
            string resp = await GetPageAsync<T>(limit, offset);
            return JsonConvert.DeserializeObject<NamedApiResourceList<T>>(resp);
        }

        /// <summary>
        /// Gets a single page of unnamed resource data
        /// </summary>
        /// <typeparam name="T">The type of resource</typeparam>
        /// <returns>The paged resource object</returns>
        public async Task<ApiResourceList<T>> GetApiResourcePageAsync<T>() where T : ApiResource
        {
            string resp = await GetPageAsync<T>();
            return JsonConvert.DeserializeObject<ApiResourceList<T>>(resp);
        }

        private async Task<string> GetPageAsync<T>(int? limit = null, int? offset = null) where T : ResourceBase
        {
            string apiEndpoint = GetApiEndpointString<T>();
            string queryParameters = String.Empty;

            // limit and offset cannot both be null
            if (limit != null)
            {
                queryParameters = $"?limit={limit}";
                if (offset != null)
                {
                    queryParameters += $"&offset={offset}";
                }
            }

            HttpResponseMessage response = await _client.GetAsync($"{apiEndpoint}{queryParameters}");

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }



    }
}
