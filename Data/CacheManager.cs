﻿using PokeProject.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace PokeProject.Data
{
    internal class CacheManager
    {
        private readonly Dictionary<Type, GenericCache<ResourceBase>> _allCaches;
        private readonly List<Type> ApiTypes = Assembly.GetExecutingAssembly().GetTypes()
        .Where(type => type.IsSubclassOf(typeof(ApiResource)) || type.IsSubclassOf(typeof(NamedApiResource)))
        .ToList();
        
        public CacheManager()
        {
            _allCaches = new Dictionary<System.Type, GenericCache<ResourceBase>>();
            foreach (System.Type type in ApiTypes)
            {
                _allCaches.Add(type, new GenericCache<ResourceBase>());
            }
        }

        public T Get<T>(int id) where T : ResourceBase
        {
            Type type = typeof(T);
            _allCaches[type].Cache.TryGetValue(id, out ResourceBase value);
            return value as T;
        }

         public T Get<T>(string name) where T : ResourceBase
        {
            Type type = typeof(T);
            PropertyInfo nameProperty = type.GetProperties()
                .FirstOrDefault(property => property.Name.Equals("Name"));
            if (nameProperty == null)
            {
                return null;
            }

            ResourceBase matchingObject = null;
            foreach (ResourceBase cacheObj in _allCaches[type].Cache.Values)
            {
                // we wouldn't be here without knowing that T has a Name property
                string value = nameProperty.GetValue(cacheObj) as string;
                if (value.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                {
                    matchingObject = cacheObj;
                    break;
                }
            }

            return matchingObject as T;
        }

        public void Store<T>(T obj) where T : ResourceBase
        {
            Type matchingType = ApiTypes.FirstOrDefault(type => type == obj.GetType());
            if (matchingType == null)
            {
                throw new NotSupportedException();
            }

            _allCaches[matchingType].Store(obj);
        }


        private class GenericCache<T> where T : ResourceBase
        {
            /// <summary>
            /// The underlying data store for the cache
            /// </summary>
            public readonly ConcurrentDictionary<int, T> Cache;

            public GenericCache()
            {
                Cache = new ConcurrentDictionary<int, T>();
            }

          
            public void Store(T obj)
            {
                Cache.TryAdd(obj.Id, obj);
            }

            
            public void Clear()
            {
                Cache.Clear();
            }
        }
    }

}
