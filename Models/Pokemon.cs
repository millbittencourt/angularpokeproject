﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PokeProject.Models
{
    [Table("pokemon")]
    public class Pokemon : NamedApiResource
    {
        public Pokemon(int id, string name, int height, string speciesname, int treinadorId)
        {
            this.Name = name;
            this.Height = height;
            this.SpeciesName = speciesname;
            this.TreinadorId = treinadorId;
        }

        public Pokemon(){ }

        // [Column("id")]
        // public int? IdPK { get; set; }

        [Column("id")]
        public override int Id { get; set; }

        [NotMapped]
        internal new static string ApiEndpoint { get; } = "pokemon";

        [Column("name")]
        public override string Name { get; set; }
        [Column("height")]
        public int Height { get; set; }

        [NotMapped]
        public PokeSpecies Species { get; set; }

        [Column("speciesname")]
        public string SpeciesName
        {
            get { return Species != null ? Species.Name : string.Empty; }
            set { }
        }

        [NotMapped]
        public PokeColor Colors { get; set; }

        [NotMapped]
        // [Column("color")]
        public string Color
        {
            get { return Colors != null ? Colors.Name : string.Empty; }
            set { }
        }

        [Column("treinadorid")]
        [ForeignKey("Treinador")]
        public int? TreinadorId { get; set; }
        public Treinador Treinador { get; set; }

    }

    public class PokeSpecies : NamedApiResource
    {
        public override int Id { get; set; }
        internal new static string ApiEndpoint { get; } = "pokemon-species";
        public override string Name { get; set; }
    }

    public class PokeColor : NamedApiResource
    {
        public override int Id { get; set; }

        internal new static string ApiEndpoint { get; } = "pokemon-color";

        public override string Name { get; set; }

        [JsonProperty("pokemon_species")]
        public List<NamedApiResource<PokeSpecies>> PokemonSpecies { get; set; }
    }

}
