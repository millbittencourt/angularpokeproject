﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PokeProject.Models
{
    public abstract class UrlNavigation<T> where T : ResourceBase
    {
        public string Url { get; set; }
    }
}
