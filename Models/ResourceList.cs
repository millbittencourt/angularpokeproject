﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PokeProject.Models
{
    public abstract class ResourceList<T> where T : ResourceBase
    {
        public int Count { get; set; }
    }

    public class ApiResourceList<T> : ResourceList<T> where T : ApiResource
    {
        public List<ApiResource<T>> Results { get; set; }
    }

    public class NamedApiResourceList<T> : ResourceList<T> where T : NamedApiResource
    {
       public List<NamedApiResource<T>> Results { get; set; }
    }
}
