﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PokeProject.Models
{
    [Table("treinador")]
    public class Treinador
    {
        [Column("id")]
        [Required]
        public int Id
        {
            get; set;
        }

        [Column("name")]
        [Required]
        public string Name
        {
            get; set;
        }

        [Column("email")]
        [Required]
        public string Email
        {
            get; set;
        }

            // [ForeignKey("TreinadorId")]
        public ICollection<Pokemon> Pokemons { get; set; }
    }
}
