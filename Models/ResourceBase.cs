﻿namespace PokeProject.Models
{
       public abstract class ResourceBase
    {
       public abstract int Id { get; set; }

    
        public static string ApiEndpoint { get; }
    }

       public abstract class NamedApiResource : ResourceBase
    {
      // [NotMapped]
       public abstract string Name { get; set; }
    }

    public class NamedApiResource<T> : UrlNavigation<T> where T : ResourceBase
    {
        public string Name { get; set; }
    }

    public class ApiResource<T> : UrlNavigation<T> where T : ResourceBase { }

    public abstract class ApiResource : ResourceBase { }

}
